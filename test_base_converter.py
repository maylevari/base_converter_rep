import pytest
from base_converter import generate_base_characters
from base_converter import string_base_checking
from base_converter import base_validation
from base_converter import base_converting


def test_generate_base_characters_2():
    base_characters_list = ['0', '1']
    assert base_characters_list == generate_base_characters(2)


def test_generate_base_characters_16():
    base_characters_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    assert base_characters_list == generate_base_characters(16)


def test_generate_base_characters_36():
    base_characters_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    assert base_characters_list == generate_base_characters(36)


def test_string_base_checking_in_base_16():
    input_validation_result = string_base_checking("DeadBEeF", 16)
    assert input_validation_result is True


def test_string_base_checking_not_in_base_16():
    input_validation_result = string_base_checking("DeadBEeG", 16)
    assert input_validation_result is False


def test_string_base_checking_in_base_2():
    input_validation_result = string_base_checking("010111", 2)
    assert input_validation_result is True


def test_string_base_checking_not_in_base_2():
    input_validation_result = string_base_checking("01212", 2)
    assert input_validation_result is False


def test_base_validation_not_a_legal_base_less_than_2():
    base_validation_result = base_validation(16, 1)
    assert base_validation_result == False


def test_base_validation_not_a_legal_base_fraction():
    base_validation_result = base_validation(1.5, 10)
    assert base_validation_result == False


def test_base_validation_is_a_legal_base():
    base_validation_result = base_validation(36, 2)
    assert base_validation_result == True


def test_base_converter_base_16_to_10():
    assert base_converting("DEADBEEF", 16, 10) == "3735928559"


def test_base_converter_base_2_to_8():
    assert base_converting("11010111", 2, 8) == "327"


def test_base_converter_base_8_to_16():
    assert base_converting("5467", 8, 16) == "B37"


def test_base_converter_base_36_to_16():
    assert base_converting("1PS9wXB", 36, 16) == "DEADBEEF"


def test_base_converter_base_37_to_16():
    assert base_converting("1PS9wXB", 37, 16) is None
