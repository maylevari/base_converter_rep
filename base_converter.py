FIRST_DIGIT = '0'
LAST_DIGIT = '9'
FIRST_CHARACTER = 'A'
LAST_CHARACTER = 'Z'


def string_base_checking(num_as_string, src_base):
    num_as_string = num_as_string.upper()  # changing the lower case to upper case
    base_characters_list = generate_base_characters(src_base)
    result = [character for character in num_as_string if (
            character not in base_characters_list)]  # iterating over num_as_string checking if there are characters that are no in the base_characters_list using list comprehension
    if len(result) != 0:  # checking if all characters where in the base_characters_list
        print "The number ", num_as_string, " is not a legal number in base ", src_base
        return False
    return True


def base_validation(src_base, dst_base):
    if type(src_base) != int or src_base < 2 or src_base > 36:
        print("a base number must be a hole number between 2 to 36")
        return False
    if type(dst_base) != int or dst_base < 2 or dst_base > 36:
        print("a base number must be a hole number between 2 to 36")
        return False
    return True


def generate_base_characters(src_base):
    base_characters_list = []
    for i in range(0, src_base):
        base_characters_list.append(ord(FIRST_DIGIT) + len(base_characters_list))
    base_characters_list = list(map(lambda x: chr(x), base_characters_list))
    base_characters_list = list(
        filter(lambda x: (x >= FIRST_DIGIT and x <= LAST_DIGIT) or (x >= FIRST_CHARACTER and x <= LAST_CHARACTER),
               base_characters_list))
    while len(base_characters_list) < src_base:
        base_characters_list.append(chr(ord(FIRST_CHARACTER) + len(base_characters_list) - 10))
    return base_characters_list


def base_converting(num_as_string, src_base, dst_base):
    if base_validation(src_base, dst_base) == False:
        return None
        exit(-1)
    input_validation_answer = string_base_checking(num_as_string, src_base)
    if input_validation_answer == False:
        return None
        exit(-1)
    decimal_number = to_decimal(num_as_string, src_base)
    final_result = from_decimal(decimal_number, dst_base)
    return final_result


def to_decimal(num_as_string, src_base):
    num_as_string = num_as_string.upper()
    base_characters_list = generate_base_characters(src_base)
    decimal_number = 0
    power = len(num_as_string) - 1
    for character in num_as_string:
        decimal_number += base_characters_list.index(character) * src_base ** power
        power -= 1
    return decimal_number


def from_decimal(decimal_number, dst_base):
    base_characters_list = generate_base_characters(dst_base)
    dst_base_number_list = []
    remainder = decimal_number % dst_base
    dst_base_number_list.insert(0, base_characters_list[remainder])
    quotient = decimal_number / dst_base
    while quotient > 0:
        remainder = quotient % dst_base
        quotient = quotient / dst_base
        dst_base_number_list.insert(0, base_characters_list[remainder])
    dst_base_number = ''
    return dst_base_number.join(dst_base_number_list)


def main():
    num_as_string = raw_input("please enter a number in any base you like: ")
    src_base = input("Please enter the source base of the number you just entered: ")
    dst_base = input("Please enter the destination base you need: ")
    result = base_converting(num_as_string, src_base, dst_base)
    print num_as_string, "is a valid number in base", src_base, "Its representation in destination base (", dst_base, ") is: ", result


if __name__ == '__main__':
    main()
